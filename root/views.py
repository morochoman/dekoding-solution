from django.shortcuts import render
from django.views.generic import View
from .models import DogModel, OwnerModel
from .forms import AddDogForm, AddOwnerForm
from django.urls import reverse

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect


class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

class DogListView(View):
    template_name = "dog/list.html"

    def get(self, request):
        dogs = DogModel.objects.all()
        return render(request, self.template_name, {'dogs':dogs})

def AddDog(request):
    newDog = DogModel()
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = AddDogForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            newDog.dogName = form.cleaned_data['dog_name']
            newDog.dogAge = form.cleaned_data['dog_age']
            newDog.dogBreed = form.cleaned_data['dog_breed']
            newDog.dogOwner = form.cleaned_data['dog_owner']
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            newDog.save()

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('DogList') )

    # If this is a GET (or any other method) create the default form.
    else:
        form = AddDogForm()

    return render(request, 'dog/add.html', {'form': form})

class OwnerListView(View):
    template_name = "owner/list.html"

    def get(self, request):
        owners = OwnerModel.objects.all()
        return render(request, self.template_name, {'owners':owners})

def AddOwner(request):
    newOwner = OwnerModel()
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = AddOwnerForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            newOwner.dni = form.cleaned_data['owner_dni']
            newOwner.firstName = form.cleaned_data['owner_firstName']
            newOwner.secondName = form.cleaned_data['owner_secondName']
            newOwner.address = form.cleaned_data['owner_address']
            newOwner.province = form.cleaned_data['owner_province']
            newOwner.department = form.cleaned_data['owner_department']
            newOwner.nationality = form.cleaned_data['owner_nationality']
            newOwner.cellular = form.cleaned_data['owner_cellular']
            newOwner.email = form.cleaned_data['owner_email']
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            newOwner.save()

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('OwnerList'))

    # If this is a GET (or any other method) create the default form.
    else:
        form = AddOwnerForm()

    return render(request, 'owner/add.html', {'form': form})
