from django.urls import path
from django.conf.urls import include, url

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    DogListView,
    OwnerListView,
)
from root import views


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/list', DogListView.as_view(), name="DogList"),
    url(r'^dog/add/$', views.AddDog, name='add_dog'),
    path('owner/list', OwnerListView.as_view(), name="OwnerList"),
    url(r'^owner/add/$', views.AddOwner, name='add_owner'),
]
