# Generated by Django 2.1.5 on 2019-01-24 01:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('root', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OwnerModel',
            fields=[
                ('dni', models.CharField(max_length=8, primary_key=True, serialize=False)),
                ('firstName', models.CharField(max_length=50)),
                ('secondName', models.CharField(max_length=50)),
                ('address', models.CharField(max_length=200)),
                ('province', models.CharField(max_length=150)),
                ('department', models.CharField(max_length=150)),
                ('nationality', models.CharField(max_length=150)),
                ('cellular', models.CharField(max_length=12)),
                ('email', models.CharField(max_length=100)),
            ],
        ),
    ]
