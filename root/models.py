from django.db import models


# Create your models here.
class DogModel(models.Model):
    dogName = models.CharField(max_length=30)
    dogBreed = models.CharField(max_length=50)
    dogAge = models.IntegerField(default=0)
    dogOwner = models.CharField(max_length=150)

    def __str__(self):
        return self.dogName
    
    def get_absolute_url(self):
        return reverse('dog-detail', args=[str(self.id)])


class OwnerModel(models.Model):
    dni = models.CharField(max_length=8, primary_key=True)
    firstName = models.CharField(max_length=50)
    secondName = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    province = models.CharField(max_length=150)
    department = models.CharField(max_length=150)
    nationality = models.CharField(max_length=150)
    cellular = models.CharField(max_length=12)
    email = models.CharField(max_length=100)

    def __str__(self):
        return self.dni
    
    def get_absolute_url(self):
        return reverse('owner-detail', args=[str(self.dni)])
