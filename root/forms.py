from django import forms

from .models import DogModel, OwnerModel

'''class DogForm(forms.ModelForm):

    class Meta:
        model = DogModel
        fields = ['dogName', 'dogAge', 'dogBreed', 'dogOwner']'''

class AddDogForm(forms.Form):
    dog_name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    dog_age = forms.IntegerField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    dog_breed = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    dog_owner = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))

    def clean_dog_data(self):
        data = self.cleaned_data['dog_name']
        data += self.cleaned_data['dog_age']
        data += self.cleaned_data['dog_breed']
        data += self.cleaned_data['dog_owner']
        return data

class AddOwnerForm(forms.Form):

    owner_dni =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_firstName =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_secondName =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_address =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_province =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_department =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_nationality =forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_cellular =forms.IntegerField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    owner_email =forms.EmailField(widget=forms.TextInput(attrs={'class' : 'form-control'}))

    def clean_dog_data(self):
        data = self.cleaned_data['owner_dni']
        data += self.cleaned_data['owner_firstName']
        data += self.cleaned_data['owner_secondName']
        data += self.cleaned_data['owner_address']
        data += self.cleaned_data['owner_province']
        data += self.cleaned_data['owner_department']
        data += self.cleaned_data['owner_nationality']
        data += self.cleaned_data['owner_cellular']
        data += self.cleaned_data['owner_email']
        return data
