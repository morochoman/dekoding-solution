from django.contrib import admin
from .models import DogModel

# Register your models here.
admin.site.register(DogModel)